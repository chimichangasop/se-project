<?php
Class User extends CI_Model
{
    function login($username, $password)
    {
        $this -> db -> select('id, username, password');
        $this -> db -> from('users');
        $this -> db -> where('username', $username);
        $this -> db -> where('password', MD5($password));
        $this -> db -> limit(1);

        $query = $this -> db -> get();

        if($query -> num_rows() == 1)
        {
            return $query->result();
        }
        else
        {
            return false;
        }
    }

    function update_password_by_email($email,$password)
    {
        $data = array('password' => $password );
        $this->db->select('username');
        $this->db->from('users');
        $this->db->join('personal_data','users.idPersonalData = personal_data.id');
        $this->db->where('personal_data.email',$email);
        $this->db->limit(1);
        $query = $this->db->get();
        if($query -> num_rows() == 1)
        {
            foreach ($query->result() as $row) {
                $username = $row->username;
            }
        }
        else
        {
            return false;
        }
        $this->db->where('username',$username);
        $this->db->update('users',$data);

    }

    function update_password($username,$password)
    {
        $data = array('password' => $password );
        $this->db->where('username',$username);
        $this->db->update('users',$data);
    }


    function login_with_cookie($username, $password)
    {
        $this -> db -> select('id, username, password');
        $this -> db -> from('users');
        $this -> db -> where('username', $username);
        $this -> db -> where('password', $password);
        $this -> db -> limit(1);

        $query = $this -> db -> get();

        if($query -> num_rows() == 1)
        {
            return $query->result();
        }
        else
        {
            return false;
        }
    }

    function get_user_role($username){
        $this -> db -> select("roles.id from users inner join roles on roles.id=users.idRole where username='".$username."' limit 1");
        $query = $this -> db -> get();
        if($query -> num_rows() == 1)
        {
            return $query->result()[0]->id;
        }
        else
        {
            return false;
        }
    }

    function get_user_id($username)
    {
        $this->db->select('id');
        $this->db->from('users');
        $this->db->where('username',$username);
        $this->db->limit(1);
        $query = $this->db->get();
        if($query -> num_rows() == 1)
        {
            return $query->result()[0]->id;
        }
        else
        {
            return false;
        }
    }

    function get_user_first_name($username)
    {
        $this->db->select('personal_data.first_name');
        $this->db->from('users');
        $this->db->join('personal_data','users.idPersonalData = personal_data.id');
        $this->db->where('users.username',$username);
        $this->db->limit(1);
        $query = $this->db->get();
        if($query -> num_rows() == 1)
        {
            return $query->result()[0]->first_name;
        }
        else
        {
            return false;
        }
    }

    function get_user_last_name($username)
    {
        $this->db->select('personal_data.last_name');
        $this->db->from('users');
        $this->db->join('personal_data','users.idPersonalData = personal_data.id');
        $this->db->where('users.username',$username);
        $this->db->limit(1);
        $query = $this->db->get();
        if($query -> num_rows() == 1)
        {
            return $query->result()[0]->last_name;
        }
        else
        {
            return false;
        }
    }

    function get_user_birth_date($username)
    {
        $this->db->select('personal_data.birth_date');
        $this->db->from('users');
        $this->db->join('personal_data','users.idPersonalData = personal_data.id');
        $this->db->where('users.username',$username);
        $this->db->limit(1);
        $query = $this->db->get();
        if($query -> num_rows() == 1)
        {
            return $query->result()[0]->birth_date;
        }
        else
        {
            return false;
        }
    }

    function get_user_email($username)
    {
        $this->db->select('personal_data.email');
        $this->db->from('users');
        $this->db->join('personal_data','users.idPersonalData = personal_data.id');
        $this->db->where('users.username',$username);
        $this->db->limit(1);
        $query = $this->db->get();
        if($query -> num_rows() == 1)
        {
            return $query->result()[0]->email;
        }
        else
        {
            return false;
        }
    }

    function get_user_phone_number($username)
    {
        $this->db->select('personal_data.phone_number');
        $this->db->from('users');
        $this->db->join('personal_data','users.idPersonalData = personal_data.id');
        $this->db->where('users.username',$username);
        $this->db->limit(1);
        $query = $this->db->get();
        if($query -> num_rows() == 1)
        {
            return $query->result()[0]->phone_number;
        }
        else
        {
            return false;
        }
    }

    function get_user_cnp($username)
    {
        $this->db->select('personal_data.cnp');
        $this->db->from('users');
        $this->db->join('personal_data','users.idPersonalData = personal_data.id');
        $this->db->where('users.username',$username);
        $this->db->limit(1);
        $query = $this->db->get();
        if($query -> num_rows() == 1)
        {
            return $query->result()[0]->cnp;
        }
        else
        {
            return false;
        }
    }

    function check_email($email)
    {
        $this->db->select('email');
        $this->db->from('personal_data');
        $this->db->where('email',$email);
        $this->db->limit(1);
        $query = $this->db->get();
        if($query -> num_rows() == 1)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    function get_user_group_id($username)
    {
        $this->db->select('groups.id');
        $this->db->from('users');
        $this->db->join('groups','users.idGroup = groups.id');
        $this->db->where('users.username',$username);
        $this->db->limit(1);
        $query = $this->db->get();
        if($query -> num_rows() == 1)
        {
            return $query->result()[0]->id;
        }
        else
        {
            return false;
        }
    }

    function get_user_role_description($username)
    {
        $this->db->select('roles.role');
        $this->db->from('users');
        $this->db->join('roles','users.idRole = roles.id');
        $this->db->where('users.username',$username);
        $this->db->limit(1);
        $query = $this->db->get();
        if($query -> num_rows() == 1)
        {
            return $query->result()[0]->role;
        }
        else
        {
            return false;
        }
    }

    function get_user_faculty($username)
    {
        $this->db->select('faculties.name');
        $this->db->from('faculties');
        $this->db->join('users','users.idFaculty = faculties.id');
        $this->db->where('users.username',$username);
        $this->db->limit(1);
        $query = $this->db->get();
        if($query -> num_rows() == 1)
        {
            return $query->result()[0]->name;
        }
        else
        {
            return false;
        }
    }

    function get_user_university($username)
    {
        $this->db->select('universities.name');
        $this->db->from('universities');
        $this->db->join('faculties','faculties.idUniversity = universities.id');
        $this->db->join('users','users.idFaculty = faculties.id');
        $this->db->where('users.username',$username);
        $this->db->limit(1);
        $query = $this->db->get();
        if($query -> num_rows() == 1)
        {
            return $query->result()[0]->name;
        }
        else
        {
            return false;
        }
    }

    function get_user_specialization($username)
    {
        $this->db->select('specializations.name');
        $this->db->from('specializations');
        $this->db->join('groups','groups.idSpecialization = specializations.id');
        $this->db->join('users','users.idGroup = Groups.id');
        $this->db->where('users.username',$username);
        $this->db->limit(1);
        $query = $this->db->get();
        if($query -> num_rows() == 1)
        {
            return $query->result()[0]->name;
        }
        else
        {
            return false;
        }
    }
    function update_user_email($username,$email)
    {
        $data = array('email' => $email );
        $this->db->select('personal_data.id');
        $this->db->from('personal_data');
        $this->db->join('users','users.idPersonalData = personal_data.id');
        $this->db->where('username',$username);
        $this->db->limit(1);
        $query = $this->db->get();
        if($query -> num_rows() == 1)
            $personal_data_id = $query->result()[0]->id;
        else
            return 0;
        $this->db->where('id',$personal_data_id);
        $this->db->update('personal_data',$data);
        return $this->db->affected_rows();
    }

    function update_user_phone_number($username,$phone_number)
    {
        $data = array('phone_number' => $phone_number );
        $this->db->select('personal_data.id');
        $this->db->from('personal_data');
        $this->db->join('users','users.idPersonalData = personal_data.id');
        $this->db->where('username',$username);
        $this->db->limit(1);
        $query = $this->db->get();
        if($query -> num_rows() == 1)
            $personal_data_id = $query->result()[0]->id;
        else
            return 0;
        $this->db->where('id',$personal_data_id);
        $this->db->update('personal_data',$data);
        return $this->db->affected_rows();
    }


}
?>
