<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Academic Info - Private Area</title>
    <!-- User defined CSS -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/styles.css');?>"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/home.css');?>"/>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css">
    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <!-- Latest compiled JavaScript -->
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>
    <div id="home-page">
        <div id="home-header-container">
            <div id="header-logo"></div>
            <div id="home-top-submenu-container">
                <ul id="home-top-submenu-list">
                    <li><input type="button" class="top-submenu-button" value="button3"/></li>
                    <li><input type="button" class="top-submenu-button" value="button2"/></li>
                    <li><input type="button" class="top-submenu-button" value="button1"/></li>
                </ul>
            </div>
            <div id="top-right-content-container">
                <div class="dropdown">
                    <span class="droptrigger inner-addon glyphicon glyphicon-user"></span>
                    <div class="dropdown-content">
                        <div><a href="#">Account Information</a></div>
                    </div>
                </div>
            </div>
        </div>
        <div id="home-content-container">
            <div id="home-left-submenu-container">
                <ul id="home-left-submenu-list">
                    <li><input type="button" class="left-submenu-button" value="Button1"/></li>
                    <li><input type="button" class="left-submenu-button" value="Button2"/></li>
                    <li><input type="button" class="left-submenu-button" value="Button3z`"/></li>
                </ul>
            </div>
            <div id="home-page-content-container">
                <!--Page gets inserted here-->
            </div>
        </div>
    </div>
</body>
</html>
