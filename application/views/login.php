<!DOCTYPE html>
<html>
<head>
	<meta http-equiv='Content-Type' content='text/html' charset='utf-8' />
	<title>Academic Info</title>

    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/login.css');?>"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/styles.css');?>"/>

    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css">

    <!-- <script src="/assets/jquery-2.1.1.min.js"></script> -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

</head>
<body>

<div id="login-page">
    <section id="small-form-content" role="main">
        <div id="main-login-content">
            <div id="main-login-form">
                <div class="ubb-logo">
                    <h1>LOGIN</h1>
                </div>
                <div class="login-form">
                    <?php
                        $error_message = validation_errors();
                        if ("" != $error_message)
                        {
                            echo '<div class="alert alert-danger">';
                            echo $error_message;
                            echo '</div>';
                        }
                    ?>
                    <?php echo form_open('verifylogin'); ?>
                    <div class="inner-addon left-addon">
                        <i class="glyphicon glyphicon-user"></i>
                        <input type="text" class="form-control" size="40" id="username" placeholder="Username" name="username"/>
                    </div>
                    <br/>
                    <div class="inner-addon left-addon">
                        <i class="glyphicon glyphicon-lock"></i>
                        <input type="password" class="form-control" size="40" id="password" placeholder="Password" name="password"/>
                    </div>
                    <br/>
                    <input type="checkbox" class="login-inline-info-left login-remember" name="remember_me" value="Remember">
                        <div class=login-remember-text>
                            Remember
                        </div>
                    </input>
                    <a href="recovery" class="login-inline-info-right">Forgot password</a>
                    <br/><br/>
                    <input type="submit" class="btn btn-default" value="Login"/>
                    <br/>
                    </form>
                </div>
            </div>
        </div>
    </section>
</div>

<?php $this->load->view('footer'); ?>
