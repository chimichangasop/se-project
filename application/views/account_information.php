<?php $this->load->view('header'); ?>

<?php $this->load->view('left'); ?>

<div class="home-page-content-container">
    <h1>Account Information</h1>
<?php echo validation_errors(); ?>
<?php echo $message; ?>
<?php echo form_open('accountinformation'); ?>
    <label for="university">University:</label>
    <input type="text" size="20" id="university" name="university" value='<?php echo $university; ?>' readonly/><br/>
    <label for="faculty">Faculty:</label>
    <input type="text" size="20" id="faculty" name="faculty" value='<?php echo $faculty; ?>' readonly/><br/>
    <label for="specialization">Specialization:</label>
    <input type="text" size="20" id="specialization" name="specialization" value='<?php echo $specialization; ?>' readonly/><br/>
    <label for="user_type">User type:</label>
    <input type="text" size="20" id="user_type" name="user_type" value='<?php echo $user_type; ?>' readonly/><br/>
    <label for="group">Group:</label>
    <input type="text" size="20" id="group" name="group" value='<?php echo $group; ?>' readonly/><br/>

    <label for="birth_date">Birth Date:</label>
    <input type="text" size="20" id="birth_date" name="birth_date" value='<?php echo $birth_date; ?>' readonly/><br/>
    <label for="security_number">Social Security Number:</label>
    <input type="text" size="20" id="security_number" name="security_number" value='<?php echo $cnp; ?>' readonly/><br/>

    <br/>
    <label for="first_name">First Name:</label>
    <input type="text" size="20" id="first_name" name="first_name" value='<?php echo $first_name; ?>' readonly/><br/>
    <label for="last_name">Last Name:</label>
    <input type="text" size="20" id="last_name" name="last_name" value='<?php echo $last_name; ?>' readonly/><br/>
    <label for="email">Email Address:</label>
    <input type="text" size="20" id="email" name="email" value='<?php echo $email; ?>'/><br/>
    <label for="phone_number">Phone Number:</label>
    <input type="text" size="20" id="phone_number" name="phone_number" value='<?php echo $phone_number; ?>'/><br/>
    <br/>

    Change Password</br>
    <label for="old_password">Old Password:</label>
    <input type="password" size="20" id="old_password" name="old_password"/><br/>
    <label for="new_password">New Password:</label>
    <input type="password" size="20" id="new_password" name="new_password"/><br/>
    <label for="new_password2">New Password Again:</label>
    <input type="password" size="20" id="new_password2" name="new_password2"/><br/>

    <input type="submit" value="Update"/>
</form>

</div>

<?php $this->load->view('footer'); ?>
