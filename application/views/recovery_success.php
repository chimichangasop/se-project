<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Academic INFO Account Recovery</title>
        <!-- User defined CSS -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/styles.css');?>"/>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/recovery.css');?>"/>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css">
        <!-- jQuery library -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
        <!-- Latest compiled JavaScript -->
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    </head>
    <body>
        <div id="recovery-page">
            <section id="small-form-content" role="main">
                <div id="main-recovery-content">
                    <div id="main-recovery-form">
                        <div class="ubb-logo">
                            <h1>LOGIN</h1>
                        </div>
                        <div class="recovery-form">
                            <div class="alert alert-success">
                                An email has been sent to <b><i><?php echo $email; ?></i></b> containing your new password.
                            </div>
                            <br/>
                            <a href="login">Login page</a>
                            </form>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </body>
</html>
