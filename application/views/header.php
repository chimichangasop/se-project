<!DOCTYPE html>
<html>
<head>
	<meta http-equiv='Content-Type' content='text/html' charset='utf-8' />
	<title>Academic Info</title>

    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/styles.css');?>"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/home.css');?>"/>
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css">

    <!-- <script src="/assets/jquery-2.1.1.min.js"></script> -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

</head>
<body>
    <h1>
        <?php echo $role_name; ?>
    </h1>
    <div id="home-header-container"> <!-- de scos home pt ca e general-->
        <div id="header-logo"></div>
        <div id="home-top-menu-container">
            <div class="inner-addon right-addon top-menu-item">
                <i class="glyphicon glyphicon-user"></i>
            </div>
            <a href="home" class="top-menu-button">Home</a>
            <a href="accountinformation" class="top-menu-button">Account information</a>
        <?php if($role == 1) { ?>
            <a href="optionalcourses" class="top-menu-button">Optional courses</a>
        <?php } ?>
            <a href="home/logout" class="top-menu-button">Logout</a>
        </div>
    </div>
    <div id="home-content-container">
