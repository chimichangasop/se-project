<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if (!function_exists('get_user_details')) {
    function get_user_details() {
        $ci =& get_instance();
        $session_data = $ci->session->userdata('logged_in');
        $data['username'] = $session_data['username'];

        $role = $ci->session->userdata('role');

        $data['role'] = $role;

        if ($role == 1)
            $data['role_name'] = 'Role 1, Student';
        elseif ($role == 2)
            $data['role_name'] = 'Role 2, Teacher';
        elseif ($role == 3)
            $data['role_name'] = 'Role 3, Head of department';
        elseif ($role == 4)
            $data['role_name'] = 'Role 4, Secretary';
        elseif ($role == 5)
            $data['role_name'] = 'Role 5, Admin';

        return $data;
    }
}
