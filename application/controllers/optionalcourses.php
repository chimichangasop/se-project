<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class OptionalCourses extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('user_details');
        if (!$this->session->userdata('logged_in')) {
            redirect('/login', 'refresh');
        }
        elseif ($this->session->userdata('role') != 1) {
            redirect('/home', 'refresh');
        }
    }

    function index() {
        $data = get_user_details();
        $data['left'] = 'optionalcourses';
        $this->load->view('account_information2', $data);
    }

    function button2() {
        $data = get_user_details();
        $data['left'] = 'optionalcourses';
        $this->load->view('account_information3', $data);
    }

}
