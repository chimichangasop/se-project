<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Recovery extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('user','',true);
        $this->load->model('emailhelper','',true);
        $this->load->helper('string');
    }

    function index()
    {
        $this->load->helper(array('form'));
        $this->load->library('form_validation');

        $this->form_validation->set_rules('email', 'Email', 'trim|required|callback_check_email');

        if($this->form_validation->run() == false)
            $this->load->view('recovery');
    }

    function check_email()
    {
        $email = $this->input->post('email');
        $result = $this->user->check_email($email);

        if($result)
        {
            $new_password = random_string('alnum', 6);
            if($this->emailhelper->sendMail($email,"Academic INFO Password Recovery",'Your new password is: '.$new_password))
            {
                $this->user->update_password_by_email($email,MD5($new_password));
                $data['email'] = $email;
                $this->load->view('recovery_success',$data);
                return true;
            }
            else {
                $this->form_validation->set_message('check_email', 'Error sending email! Please try again later.');
                return false;
            }
        }
        else
        {
            $this->form_validation->set_message('check_email', 'Inexistent email address!');
            return false;
        }
    }
}
