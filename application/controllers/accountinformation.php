<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class AccountInformation extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->helper('user_details');
        if(!$this->session->userdata('logged_in')) {
            redirect('/login', 'refresh');
        }
        $this->load->model('user','',true);
    }

    function index()
    {
        $this->load->helper(array('form'));
        $this->load->library('form_validation');
        $this->form_validation->set_rules('email', 'Email', 'trim');
        $this->form_validation->set_rules('phone_number', 'Phone Number', 'trim');
        $this->form_validation->set_rules('old_password', 'Old Password', 'trim|min_length[5]|max_length[16]');
        $this->form_validation->set_rules('new_password', 'New Password', 'trim|min_length[5]|max_length[16]|matches[new_password2]');
        $this->form_validation->set_rules('new_password2', 'New Password', 'trim|min_length[5]|max_length[16]|callback_update_information');

        if($this->form_validation->run() == false)
            $this->show('');
        else
            $this->show('Information Updated Successfuly');
    }

    function show($success_message)
    {
        $data = get_user_details();
        $data['left'] = 'accountinformation';
        $session_data = $this->session->userdata('logged_in');
        $username = $session_data['username'];
        $data['message'] = $success_message;
        $data['university'] = $this->user->get_user_university($username);
        $data['faculty']=$this->user->get_user_faculty($username);
        $data['specialization']=$this->user->get_user_specialization($username);
        $data['user_type']=$this->user->get_user_role_description($username);
        $data['group']=$this->user->get_user_group_id($username);
        $data['first_name']=$this->user->get_user_first_name($username);
        $data['last_name']=$this->user->get_user_last_name($username);
        $data['birth_date']=$this->user->get_user_birth_date($username);
        $data['email']=$this->user->get_user_email($username);
        $data['phone_number']=$this->user->get_user_phone_number($username);
        $data['cnp']=$this->user->get_user_cnp($username);
        $this->load->view('account_information', $data);
    }

    function update_information()
    {

        $session_data = $this->session->userdata('logged_in');
        $username = $session_data['username'];

        $email = $this->input->post('email');
        $phone_number = $this->input->post('phone_number');
        $new_password = $this->input->post('new_password');
        $new_password2 = $this->input->post('new_password2');
        $old_password = $this->input->post('old_password');

        if($email != $this->user->get_user_email($username))
        {
            if($this->user->update_user_email($username,$email)==0)
            {
                $this->form_validation->set_message('update_information', 'Error updating email!');
                return false;
            }
        }
        if($phone_number != $this->user->get_user_phone_number($username))
        {
            if($this->user->update_user_phone_number($username, $phone_number)==0)
            {
                $this->form_validation->set_message('update_information', 'Error phone number!');
                return false;
            }
        }
        if($old_password)
        {
            if($this->user->login($username,$old_password))
                $this->user->update_password($username,MD5($new_password));
            else
            {
                $this->form_validation->set_message('update_information', 'Incorrect old password!');
                return false;
            }
        }
        return true;
    }

    function button2() {
        $data = get_user_details();
        $data['left'] = 'accountinformation';
        $this->load->view('account_information2', $data);
    }

    function button3() {
        $data = get_user_details();
        $data['left'] = 'accountinformation';
        $this->load->view('account_information3', $data);
    }
}
