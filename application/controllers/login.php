<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('user','',true);
    }

    function index()
    {
    	$this->load->helper(array('form'));
    	if(isset($_COOKIE['username']) && isset($_COOKIE['password']))
        {
    		if($this->cookie_login($_COOKIE['username'],$_COOKIE['password']))
            {
    			redirect('home', 'refresh');
    		}
    		else
            {
    			$this->load->view('login');
            }
    	}
    	else
        {
            $this->load->view('login');
        }
    }

    function cookie_login($username, $password)
    {
        $result = $this->user->login_with_cookie($username, $password);
        if($result){
            $sess_array = array();
            foreach($result as $row){
                $sess_array = array(
                    'id' => $row->id,
                    'username' => $row->username
                );
                $this->session->set_userdata('logged_in', $sess_array);
            }
            return true;
        }
        return false;
    }
}
