<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->helper('user_details');
        if(!$this->session->userdata('logged_in')) {
            redirect('/login', 'refresh');
        }
    }

    function index()
    {
        $data = get_user_details();
        $data['left'] = 'home';
        $data['content'] = 'home';
        $this->load->view('home', $data);
    }

    function logout()
    {
        $this->session->unset_userdata('logged_in');
        setcookie('username', "", time() ,"/");
        setcookie('password', "", time() ,"/");
        session_destroy();
        redirect('/', 'refresh');
    }

    function button2()
    {
        $data = get_user_details();
        $data['left'] = 'home';
        $data['content'] = 'button2';
        $this->load->view('home', $data);
    }

    function btn1() {
        echo '<b>1</b>asdasfsadfasdfsa';
    }

    function btn2() {
        $data['firstname'] = '<b>1</b>';
        $data['secondname'] = '2';
        echo json_encode($data);
    }
}
